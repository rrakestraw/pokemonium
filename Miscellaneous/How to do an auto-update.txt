In this little guide, I'll explain the steps on how to make an auto update available to the world!

1. 
Create the update package

Add all updated files in the folder where they should end up eventually!
So for instance, betaclient.jar will be in the zip's root and map 2.-49.tmx will have to be <root>/res/maps/2.-49.tmx

IMPORTANT!!! Name the update package <version>.zip!!!

2. 
Upload the update package to the updater's folder C:\xampp\htdocs\updater\patchfiles (if you lost or dont know the login info, you probably shouldnt be there. 
Ask Myth1c, sadhi or Remyoman for more).

3. 
Create a new entry in the updatelinks.ini

Example:
[1.5.1]
link=http://s1.pokemonium.com/updater/patchfiles/1.5.1.zip
forced=YES

So to put it in a template:

[version]
link=http://s1.pokemonium.com/updater/patchfiles/<version>.zip
forced=<YES if the client doesnt need to be asked for confirmation, NO if this update might be unstable and the user might want to stick to the current version until it is fixed>

4.
Add the new version number to the version log
IMPORTANT!!! This has to be the same as the version you entered in the updatelinks.ini and named the update package!



After you've uploaded the new version log, all clients who attempt to start the game will receive the new update.




IT IS IMPORTANT TO DO IT IN THIS SEQUENCE!!! AS SOON AS YOU HAVE THE NEW VERSION NUMBER IN THE VERSION LOG, CLIENTS WILL ATTEMPT TO DOWNLOAD THE UPDATE.